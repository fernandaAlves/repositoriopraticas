
public class Calculadora {
	
	public Calculadora(){
		
	}
	
	public int somar(int numero1, int numero2){
		int soma = numero1 + numero2;
		return soma;
	}
	
	public int subtrair (int numero1, int numero2){
		int subtracao = numero1 - numero2;
		return subtracao;
	}
	
	public int multiplicar ( int numero1, int numero2 ){
		int multiplicao = numero1 * numero2;
		return multiplicao;
	}
	
	public float dividir ( int numero1, int numero2){
		float divisao = numero1 / numero2;
		return divisao;
	}

}
